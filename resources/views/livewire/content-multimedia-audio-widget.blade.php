<div class="widget-box widget-color-blue ui-sortable-handle mb-4" id="widget-box-7">
    <div class="widget-header widget-header-small">
        <h6 class="widget-title smaller">Audios adjuntos</h6>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            @livewire('content-multimedia-audio::content-multimedia-audio-widget-table', ['contentId' => $contentId], key('audio-'.$contentId))
            {{--            @livewire('multimedia::media-library-images', ['contentId' => $contentId, 'type' => 'documents'], key('media-library-documents-'.$contentId))--}}
        </div>
    </div>
</div>

