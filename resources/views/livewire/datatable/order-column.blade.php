<div class="d-flex justify-content-center align-items-center">
    <span class="align-items-center badge bgc-purple-d1 pos-rel text-white radius-4 px-3">
        <span class="bgc-primary-tp4 opacity-5 position-tl h-100 w-100 radius-4"></span>
        <span class="pos-rel">
            {{$row->order_column}}
        </span>
    </span>
</div>
