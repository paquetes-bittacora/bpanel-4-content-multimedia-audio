<?php

namespace Bittacora\ContentMultimediaAudio\Commands;

use Illuminate\Console\Command;

class ContentMultimediaAudioCommand extends Command
{
    public $signature = 'content-multimedia-audio';

    public $description = 'My command';

    public function handle(): int
    {
        $this->comment('All done');

        return self::SUCCESS;
    }
}
