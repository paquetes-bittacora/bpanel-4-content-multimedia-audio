<?php

namespace Bittacora\ContentMultimediaAudio;

use Bittacora\ContentMultimediaAudio\Http\Livewire\ContentMultimediaAudioWidget;
use Bittacora\ContentMultimediaAudio\Http\Livewire\ContentMultimediaAudioWidgetTable;
use Livewire\Livewire;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Bittacora\ContentMultimediaAudio\Commands\ContentMultimediaAudioCommand;

class ContentMultimediaAudioServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('content-multimedia-audio')
            ->hasConfigFile()
            ->hasViews()
            ->hasMigration('create_content-multimedia-audio_table')
            ->hasCommand(ContentMultimediaAudioCommand::class);
    }

    public function register()
    {
        $this->app->bind('content-multimedia-audio', function($app){
            return new ContentMultimediaAudio();
        });
    }

    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views/', 'content-multimedia-audio');
        Livewire::component('content-multimedia-audio::content-multimedia-audio-widget', ContentMultimediaAudioWidget::class);
        Livewire::component('content-multimedia-audio::content-multimedia-audio-widget-table', ContentMultimediaAudioWidgetTable::class);
    }
}
