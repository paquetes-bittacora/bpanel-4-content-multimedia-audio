<?php

namespace Bittacora\ContentMultimediaAudio\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\ContentMultimediaAudio\ContentMultimediaAudio
 */
class ContentMultimediaAudio extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \Bittacora\ContentMultimediaAudio\ContentMultimediaAudio::class;
    }
}
