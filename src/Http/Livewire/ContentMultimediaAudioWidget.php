<?php

namespace Bittacora\ContentMultimediaAudio\Http\Livewire;

use Bittacora\ContentMultimediaAudio\Models\ContentMultimediaAudioModel;
use Bittacora\Multimedia\Models\Multimedia;
use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;

class ContentMultimediaAudioWidget extends Component
{
    public int $contentId;
    public ?Collection $audios = null;

    protected $listeners = ['refreshWidget' => '$refresh'];

    public function mount()
    {
        $contentMultimediaData = ContentMultimediaAudioModel::where([
            ['content_id', '=', $this->contentId]
        ])->orderBy('order_column', 'ASC')->get();

        if (!empty($contentMultimediaData)) {
            foreach ($contentMultimediaData as $key => $content) {
                /**
                 * @var ContentMultimediaAudioModel $content
                 */
                $multimedia = Multimedia::where('id', $content->multimedia_id)->with('mediaModel')->first();
                $content->setAttribute('multimedia', $multimedia);

            }

            $this->audios = $contentMultimediaData;
        }
    }

    public function render()
    {
        return view('content-multimedia-audio::livewire.content-multimedia-audio-widget')->with([
            'audios' => $this->audios,
            'contentId' => $this->contentId
        ]);
    }
}
